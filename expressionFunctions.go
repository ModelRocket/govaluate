package govaluate

/*
	Represents a function that can be called from within an expression.
	This method must return an error if, for any reason, it is unable to produce exactly one unambiguous result.
	An error returned will halt execution of the expression.
*/
type ExpressionFunction func(arguments ...interface{}) (interface{}, error)

// FunctionResolver resolves expressions
type FunctionResolver interface {
	Resolve(string) (ExpressionFunction, bool)
}

// FunctionMap is a map of functions that implements the Resolver interface
type FunctionMap map[string]ExpressionFunction

// Resolve implements the resolver interface
func (m FunctionMap) Resolve(name string) (ExpressionFunction, bool) {
	if fn, ok := m[name]; ok {
		return fn, true
	}
	return nil, false
}
